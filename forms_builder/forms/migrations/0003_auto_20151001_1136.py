# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('forms', '0002_auto_20151001_1131'),
    ]

    operations = [
        migrations.AlterField(
            model_name='field',
            name='slug',
            field=models.SlugField(default='', max_length=2000, verbose_name='Slug', blank=True),
            preserve_default=True,
        ),
    ]
